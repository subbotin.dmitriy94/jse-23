package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractSystemCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Override
    @NotNull
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return arguments.get(arg);
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    @NotNull
    public Collection<AbstractSystemCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @NotNull final String name = command.name();
        commands.put(name, command);
        if (command instanceof AbstractSystemCommand) {
            final String arg = ((AbstractSystemCommand) command).arg();
            if (arg != null) arguments.put(arg, ((AbstractSystemCommand) command));
        }
    }

}
