package com.tsconsulting.dsubbotin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@UtilityClass
public final class HashUtil {

    @NotNull
    private static final String SECRET = "14";

    @NotNull
    private static final Integer ITERATION = 88;

    @NotNull
    public static String salt(@NotNull final String value) {
        @NotNull String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @NotNull
    private static String md5(@NotNull final String value) {
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
