package com.tsconsulting.dsubbotin.tm.api.entity;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasCreated {

    @NotNull
    Date getCreateDate();

    void setCreateDate(@NotNull Date createDate);

}
