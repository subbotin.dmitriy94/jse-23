package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ICommandService;
import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@RequiredArgsConstructor
public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    @Override
    @Nullable
    public AbstractCommand getCommandByName(final @Nullable String name) throws AbstractException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
        return commandRepository.getCommandByName(name);
    }

    @Override
    @NotNull
    public AbstractCommand getCommandByArg(@Nullable final String arg) throws AbstractException {
        if (EmptyUtil.isEmpty(arg)) throw new EmptyNameException();
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    @NotNull
    public Collection<AbstractSystemCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

}
