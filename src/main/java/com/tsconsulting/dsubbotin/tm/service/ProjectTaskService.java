package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@RequiredArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @Override
    public void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        isEmpty(userId, projectId, taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        isEmpty(userId, projectId, taskId);
        taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    @NotNull
    public List<Task> findAllTasksByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        if (projectRepository.existById(userId, id)) throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(userId, id);
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        if (projectRepository.existById(userId, id)) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(userId, id);
        projectRepository.removeById(userId, id);
    }

    @Override
    public void removeProjectByIndex(@NotNull final String userId, final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Project project = projectRepository.findByIndex(userId, index);
        taskRepository.removeAllTaskByProjectId(userId, project.getId());
        projectRepository.removeByIndex(userId, index);
    }

    @Override
    public void removeProjectByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Project project = projectRepository.findByName(userId, name);
        taskRepository.removeAllTaskByProjectId(userId, project.getId());
        projectRepository.removeByName(userId, name);
    }

    private void isEmpty(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (EmptyUtil.isEmpty(taskId)) throw new EmptyIdException();
        if (projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existById(userId, taskId)) throw new TaskNotFoundException();
    }

}
