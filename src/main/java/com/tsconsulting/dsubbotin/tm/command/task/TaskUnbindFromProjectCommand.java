package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-unbind-from-project";
    }

    @Override
    @NotNull
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(currentUserId, projectId);
        TerminalUtil.printMessage("Enter task id:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(currentUserId, taskId);
        serviceLocator.getProjectTaskService().unbindTaskFromProject(currentUserId, projectId, taskId);
        TerminalUtil.printMessage("[Task untied to project]");
    }

}
