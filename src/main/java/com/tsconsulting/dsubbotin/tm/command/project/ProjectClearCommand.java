package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-clear";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove all projects.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getProjectService().clear(currentUserId);
        TerminalUtil.printMessage("[Clear]");
    }

}
