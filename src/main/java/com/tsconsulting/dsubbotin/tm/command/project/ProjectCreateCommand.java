package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-create";
    }

    @Override
    @NotNull
    public String description() {
        return "Create new project.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(currentUserId, name, description);
        TerminalUtil.printMessage("[Create]");
    }

}
