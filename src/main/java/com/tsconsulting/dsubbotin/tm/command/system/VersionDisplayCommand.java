package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.Application;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class VersionDisplayCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String name() {
        return "version";
    }

    @Override
    @NotNull
    public String arg() {
        return "-v";
    }

    @Override
    @NotNull
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        TerminalUtil.printMessage(Application.class.getPackage().getImplementationVersion());
    }

}
